<?php include_once '../partials/links.php' ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<link rel="stylesheet" href="contact.css">
<title>Contact</title>
</head>

<body>
    <?php include_once '../partials/nav.php' ?>
    <?php include_once '../partials/header.php' ?>
    <div class="background">
        <div class="container">
            <div class="row">
                <div class="col-3 home">
                    <p>Home > <b>Contact us</b></p>
                    <h4>STORE INFORMATION</h4>
                    <i class="fa fa-map-marker" aria-hidden="true"></i> Kollective Technology
                    <hr>
                    <i class="fa fa-envelope-o" aria-hidden="true"></i> Email us: kollectivetechnology@tch.com
                </div>
                <div class="col-9 location">
                    <h3>Our Location</h3>
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2875.0160193117126!2d20.3426332146344!3d43.896934944655!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4757721aaff632bd%3A0xbddc942f15d8696d!2sFakultet%20tehni%C4%8Dkih%20nauka%20u%20%C4%8Ca%C4%8Dku!5e0!3m2!1ssr!2srs!4v1654467107623!5m2!1ssr!2srs" width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
                    <h3>Drop Us A Line</h3>
                    Have a question or comment? Use the from below to send us a message or contact us by mail.
                    <form action="check-mail.php" id="form" name="form" method="POST">
                        <input list="browsers" name="browser" id="mobile" placeholder="Customer device">
                        <datalist id="browsers">
                            <option value="IPhone">
                            <option value="Xiaomi">
                        </datalist>
                        <input type="text" name="email" id="email" placeholder="your@email.com">
                        <div class="input-group input">
                            <input type="search" class="form-control rounded" aria-label="Search" aria-describedby="search-addon">
                            <button type="button" class="btn btn-danger btn2">Choose file </button>
                        </div>
                        <textarea name="textarea" id="textarea" cols="60" rows="10" placeholder="How can we help?"></textarea>
                        <br><input type="button" name="submit" id="submit" class="btn btn-danger btn-comment" value="Submit"> </input>
                    </form>
                    <ul id="form-messages">
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <?php include_once '../partials/bottom.php' ?>
    <?php include_once '../partials/footer.php' ?>
    <script src="ajax-validation.js"></script>