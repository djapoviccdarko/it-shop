<?php
require_once '../database/db.php';

class DAO {
	private $db;
	private $SELECT_FROM_ORDERS = "SELECT * FROM orders JOIN products ON products.id=orders.product_id JOIN product_types on product_types.id_product_type=orders.product_type_id JOIN order_details ON order_details.odrder_detail_id=orders.order_detail_id";
	private $SELECT_BY_ID="SELECT * FROM orders JOIN products ON products.id=orders.product_id JOIN product_types on product_types.id_product_type=orders.product_type_id JOIN order_details ON order_details.odrder_detail_id=orders.order_detail_id WHERE order_id=?";
	private $INSERT_INTO_ORDERS="INSERT INTO orders (product_type,model,address,city) VALUES (?,?,?,?)";
	private $INSERT_INTO_ORDER_DETAILS="INSERT INTO order_details (first_name,last_name,phone_number,address,city,type_of_delivery) VALUES (?,?,?,?,?,?)";
	private $DELETE_ORDER = "DELETE FROM orders WHERE order_id=?";
	private $UPDATE_ORDER = "UPDATE orders SET model=?,address=?,city=? WHERE order_id=?";
	private $SELECT_FROM_PRODUCT_TYPES = "SELECT * FROM product_types";
	private $SELECT_FROM_ORDER_DETAILS= "SELECT * FROM order_details";
	public function __construct()
	{
		$this->db = DB::createInstance();
	}

	public function selectOrders()
	{
		$statement = $this->db->prepare($this->SELECT_FROM_ORDERS);
		$statement->execute();
		$result = $statement->fetchAll();
		return $result;
	}

	public function selectOrderDetails()
	{
		$statement = $this->db->prepare($this->SELECT_FROM_ORDER_DETAILS);
		$statement->execute();
		$result = $statement->fetchAll();
		return $result;
	}

	public function selectProductTypes()
	{
		$statement = $this->db->prepare($this->SELECT_FROM_PRODUCT_TYPES);
		$statement->execute();
		$result = $statement->fetchAll();
		return $result;
	}

	public function selectOrdersById($order_id)
	{
		$statement = $this->db->prepare($this->SELECT_BY_ID);
		$statement->bindValue(1, $order_id);
		$statement->execute();
		$result = $statement->fetchAll();
		return $result;
	}
    
	public function insertOrder($type, $model, $addres,$city)
	{
		$statement = $this->db->prepare($this->INSERT_INTO_ORDERS);
		$statement->bindValue(1, $type);
		$statement->bindValue(2, $model);
		$statement->bindValue(3, $addres);
		$statement->bindValue(4, $city);
		$statement->execute();
	}

	public function insertOrderDetails($first_name, $last_name, $phone_number,$address,$city,$delivery)
	{
		$statement = $this->db->prepare($this->INSERT_INTO_ORDER_DETAILS);
		$statement->bindValue(1, $first_name);
		$statement->bindValue(2, $last_name);
		$statement->bindValue(3, $phone_number);
		$statement->bindValue(4, $address);
		$statement->bindValue(5, $city);
		$statement->bindValue(6, $delivery);
		$statement->execute();
	}

	public function deleteOrder($order_id)
	{
		$statement = $this->db->prepare($this->DELETE_ORDER);
		$statement->bindValue(1, $order_id);
		$statement->execute();
	}

	public function updateOrder($model, $address, $city,$order_id)
	{
		$statement = $this->db->prepare($this->UPDATE_ORDER);
		$statement->bindValue(1, $model);
		$statement->bindValue(2, $address);
		$statement->bindValue(3, $city);
		$statement->bindValue(4, $order_id);
		$statement->execute();
	}


}
