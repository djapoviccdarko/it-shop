<nav class="navbar">
    <div class="container ">
        <section class="nav-left-section">
            <a href="">Shipping</a>
            <a href="">FAQ</a>
            <a href="">Contact</a>
            <a href="">Order</a>
        </section>
        <section class="nav-right-section">
            <?php if (!isset($_SESSION)) session_start();
            if (isset($_SESSION["user"])) {
                echo '<p style="color:white">Welcome ' . $_SESSION['user']['username'] . '</p>';
                echo '<a href="../login-page/login-controller.php?action=logout">Logout</a>';
            } else {
                echo '<a href="../register-page/register.php">Register</a>
                    <a href="../login-page/login.php">Login</a>';
            }
            ?>

        </section>
    </div>
</nav>