<?php
require_once '../shop-page/DAO-shop.php';
$dao = new DAOSHOP();
$products = $dao->selectProducts();
if (isset($_SESSION['product'])) {
  $productbyid = $dao->selectProductsById($_SESSION['product']);
?>
  <?php include_once '../partials/links.php' ?>
  <link rel="stylesheet" href="../products-page/products.css">
  <title>Shop</title>

  </head>

  <body>
    <?php include_once '../partials/nav.php' ?>
    <?php include_once '../partials/header.php' ?>

    <?php foreach ($productbyid as $pom) ?>
    <!-- Slideshow container -->
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
              <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
              <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
              <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
              <div class="carousel-item active">
                <img src="<?= $pom['image_2'] ?>" class="d-block w-100" alt="...">
              </div>
              <div class="carousel-item">
                <img src="<?= $pom['image_3'] ?>" class="d-block w-100" alt="...">
              </div>
              <div class="carousel-item">
                <img src="<?= $pom['image_4'] ?>" class="d-block w-100" alt="...">
              </div>
            </div>
            <button class="carousel-control-prev" type="button" data-target="#carouselExampleIndicators" data-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-target="#carouselExampleIndicators" data-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </button>
          </div>
        </div>
        <div class="col-md-6">
          <h1 class="heading"><?= $pom['name'] ?> <?= $pom['model'] ?> <?= $pom['memory'] ?></h1>
          <p class="description"><?= $pom['description'] ?> <br>
          <b> Price:</b><p class=""><?= $pom['price'] ?> <br>
          Discounted product price:<p class=""><?= $pom['price']-$pom['discount'] ?> <br>

            <a href='../shop-page/cart-controller.php?action=addToCart&article=<?= serialize($pom) ?>'><button style="margin-top:15rem" type="button" class="btn btn-danger">ADD TO CART<i class="fa fa-shopping-cart" aria-hidden="true"></i></button></a>
            <a href="../shop-page/cart.php"> <button style="margin-top:15rem" type="button" class="btn btn-success">VIEW SHOPPING CART</button></a>
        </div>
      </div>
    </div>

    <?php include_once '../partials/bottom.php' ?>
    <?php include_once '../partials/footer.php' ?>

    <script>
      var products =
        <?php echo json_encode($products);

        ?>;
    </script>

  <?php } ?>