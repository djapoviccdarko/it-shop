<?php
$msg = isset($msg) ? $msg : "";
if (!isset($_SESSION)) session_start();
require_once 'DAO-shop.php';
$dao = new DAOSHOP();
$products = $dao->selectProducts();
$articles = isset($_SESSION['cart']) ? $_SESSION['cart'] : [];
?>
<?php include_once '../partials/links.php' ?>
<link rel="stylesheet" href="../shop-page/shop.css">
<title>Shop</title>
</head>

<body>
    <?php include_once '../partials/nav.php' ?>
    <?php include_once '../partials/header.php' ?>
    <div class="container cards">
        <h1>Shopping cart</h1>
        <?php if (count($articles) > 0) { ?>
            <table class="table">
                <tr>
                    <th>Name</th>
                    <th>Model</th>
                    <th>Image</th>
                    <th>Price</th>
                    <th>Price on discount</th>
                    <th>Remove</th>
                </tr>
                <?php foreach ($articles as $pom) { ?>
                    <tr>
                        <td><?= $pom['name']  ?></td>
                        <td><?= $pom['model']  ?></td>
                        <td><img src="../images/<?= $pom['image']  ?>" style="width: 100px;height:100px" alt=""></td>
                        <td><?= $pom['price']  ?> din</td>
                        <td><?= $pom['price']-$pom['discount']  ?> din</td>
                        <td><a href='../shop-page/cart-controller.php?action=removeFromCart&article=<?= serialize($pom) ?>'>REMOVE FROM CART</a></td>
                    </tr>
                    <?= $msg ?>
                <?php } ?>
            </table>
        <?php } else { ?>
            <h2>Cart is empty</h2>
        <?php }  ?>
        <a href='../shop-page/cart-controller.php?action=emptyCart'>EMPTY CART</a><br>
        <a href="../shop-page/shop.php">CONTINUE SHOPING</a><br>
        <a href="../shop-page/confirm-shopping.php">CONFIRM SHOPING</a><br>
    </div>

    <?php include_once '../partials/bottom.php' ?>
    <?php include_once '../partials/footer.php' ?>

    <script>
        var products =
            <?php echo json_encode($products);

            ?>;
    </script>