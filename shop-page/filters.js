let min_price = 2000;
let max_price = 150000;
var types = ['mobile', 'laptop', 'tv'];
$(document).ready(function () {
    showItems();
});
$('#min-price').on("change mouseclick", function () {
    min_price = parseInt($('#min-price').val());
    $('#min-price-txt').text(min_price + ' din');
    showItemsFiltered();
});
$('#max-price').on("change mouseclick", function () {
    max_price = parseInt($('#max-price').val());
    $('#max-price-txt').text(max_price + ' din');
    showItemsFiltered();
});
function showItems() {
    $("#producta").empty();
    for (let i = 0; i < products.length; i++) {
        let productPrice = parseFloat(products[i].price);
        if (productPrice <= max_price && productPrice >= min_price && types.includes(products[i].type)) {
            let item_content = `<div class="card" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);width: 15rem;text-align: center;font-family: arial;display: inline-block;margin-left:2rem;margin-bottom:2rem">
  <img src="../images/`+ products[i].image + `" style="width:200px;height:200px;">
  <h2>`+ products[i].name + `</h2>
  <h3>`+ products[i].model + `</h3>
  <p class="price" style="color: grey;font-size: 22px;"><b>`+ productPrice + ` din</b></p>
  <a href="../products-page/product-controller.php?action=product&id=`+ products[i].id + `"><p><button style="border: none;outline: 0;padding: 12px;color: white;background-color: #78ac57;text-align: center;cursor: pointer;width: 100%;font-size: 18px;opacity:0.7">View product</button></p></a>
</div>`;
            $("#producta").append(item_content);
        }
    }
}
$('input:checkbox').change(
    function () {
        if ($(this).is(':checked') || !$(this).is(':checked')) {
            showItemsFiltered()
        }
    });

function showItemsFiltered() {
    types = [];
    isChecked('mobile');
    isChecked('laptop');
    isChecked('tv');
    if (types.length == 0) {
        types.push('mobile', 'laptop', 'tv');
    }
    showItems();
}

const isChecked = (checkboxId) => {
    if (document.getElementById(checkboxId).checked == true) {
        types.push(checkboxId);
    }
}
