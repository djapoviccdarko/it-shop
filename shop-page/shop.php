<?php
$msg = isset($msg) ? $msg : "";
if (!isset($_SESSION)) session_start();
require_once 'DAO-shop.php';
$dao = new DAOSHOP();
$products = $dao->selectProducts();
$manufacturer = $dao->selectManufacturer();
$brands = $dao->selectBrands();
?>
<?php include_once '../partials/links.php' ?>
<link rel="stylesheet" href="../shop-page/shop.css">
<title>Shop</title>
</head>

<body>
    <?php include_once '../partials/nav.php' ?>
    <?php include_once '../partials/header.php' ?>
    <div class="container cards">
        <div class="row">
            <div class="col-3 home">
                <section class="left-box">
                    <h2>Categories</h2>
                    <input type="checkbox" name="mobile" id="mobile" value=""> Mobiles
                    <br>
                    <input type="checkbox" name="laptop" id="laptop"> Laptops
                    <br>
                    <input type="checkbox" name="television" id="tv"> Television
                    <br>
                </section>
                <section class="left-box">
                    <h2>Price range</h2>
                    <form id="price-range-form">
                        <label for="min-price" class="form-label">Min price: </label>
                        <span id="min-price-txt">0 din</span>
                        <input type="range" class="form-range" min="0" max="150000" id="min-price" step="1" value="0"><br>
                        <label for="max-price" class="form-label">Max price: </label>
                        <span id="max-price-txt">150000 din</span>
                        <input type="range" class="form-range" min="1" max="150000" id="max-price" step="1" value="150000">
                    </form>
                </section>

            </div>

            <div class="col-9 producta" id="producta">
            </div>
        </div>
    </div>

    <?php include_once '../partials/bottom.php' ?>
    <?php include_once '../partials/footer.php' ?>
    <script>
        var products =
            <?php echo json_encode($products);

            ?>;
    </script>
    <script src="../shop-page/filters.js"></script>